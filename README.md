# Projet Eleveur Julien

Projet D'elevage  

- Monome :  
	- Dorne Julien  
	
## Lancement

```
	java -jar KFC.jar
```
	
## Menu

- 1 : Ajouter des volailles  
	- Permet de choisir le nombre, le type et le poids des volailles.  
	- ATTENTION, il ne doit pas y avoir plus de 5 poulets, 4 canards et 3 paons et au plus 7 volailles par livraison.  
	
- 2 : Modifier le poids pour l'abattage
	- Permet de modifier le poids minimum pour l'envoi a l'abattoire des volailles.  
	- par defaut : 0 Kg.  
	
- 3 : Modifier le prix du jour
	- Permet de modifier le prix au kilo de chaque type de volaille.  
	- par defaut : 0 euro.  
	
- 4 : Modifier le poids d'une volaille  
	- Permet de modifier le poids d'une volaille choisis.  
	
- 5 : Voir le nombre de volaille par type  
	- Permet de voir le nombre de volaille par type.  
	
- 6 : Voir le total de prix des volailles abattables
	- Permet de voir le prix en euros des volailles a abattres.  
	
- 7 : Vendre une volaille
	- Permet de vendre une volaille vendable avec son prix au kilo.  
	
- 8 : Rendre un paon  
	- Permet de rendre un paon au parc.  
	
- 9 : Visualiser l'etat de la ferme	(probleme d'affichage !)
	- Permet de visualiser l'etat de la ferme avec le nombre de volaille divisee.  
		- C : canard non abattable.  
		- CC : canard abattable.  
		- P : poulet non abattable.  
		- PP : poulet abattable.  
		- Pa : paon
		
- 0 : Quitter
	- Permet de quitter le programme.
	
## Ressource

Le dossier de ressource se genere automatiquement.  
Dans ce dossier, les volailles sont generees et stocker dans des fichiers.  
Un fichier d'operation est genere une fois qu'une operation est effectuee, et est remplis au fur et a mesure.  
