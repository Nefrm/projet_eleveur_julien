package fr.afpa.interf;

public interface Abattable {
	/**
	 * Change le prix de l'animal 
	 * 
	 * @param prix	le nouveau prix
	 * @return	true si la modification a ete effectuer, false si non
	 */
	public abstract boolean changerPrix(double prix);
}
