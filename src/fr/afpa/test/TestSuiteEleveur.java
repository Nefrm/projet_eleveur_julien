package fr.afpa.test;

import junit.extensions.ActiveTestSuite;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import junit.textui.TestRunner;

public class TestSuiteEleveur extends TestCase{
	
	public TestSuiteEleveur(String name) {
		super(name);
	}
	
	public static TestSuite suite() {
		TestSuite suite = new ActiveTestSuite();
		suite.addTest(new TestSuite(TestCanard.class));
		suite.addTest(new TestSuite(TestControleGeneral.class));
		suite.addTest(new TestSuite(TestEleveur.class));
		suite.addTest(new TestSuite(TestPoulet.class));
		suite.addTest(new TestSuite(TestVolailleAbattable.class));
		suite.addTest(new TestSuite(TestMenu.class));
		return suite;
	}
	
	public static void main(String[] args) {
		TestRunner.runAndWait(suite());
	}

}
