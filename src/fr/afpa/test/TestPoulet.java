package fr.afpa.test;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import fr.afpa.entite.Poulet;
import fr.afpa.service.Service;
import junit.framework.TestCase;

public class TestPoulet extends TestCase{

	Poulet poulet;

	@BeforeEach
	public void setUp() throws Exception {
		poulet = new Poulet();
		Service.initialisation();

	}

	@AfterEach
	public void tearDown() throws Exception {
	}

	@Test
	public void testChangerPrixOK() {
		assertTrue(poulet.changerPrix(10.0));
	}
	
	@Test
	public void testChangerPrixKO() {
		assertFalse(poulet.changerPrix(-10.0));
	}

	@Test
	public void testModifierPoidsAbattableOK() {
		assertTrue(poulet.modifierPoidsAbattage(10.0f));
	}
	
	@Test
	public void testModifierPoidsAbattableKO() {
		assertFalse(poulet.modifierPoidsAbattage(-10.0f));
	}
}
