package fr.afpa.test;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import fr.afpa.entite.Canard;
import fr.afpa.entite.FermeElevage;
import fr.afpa.entite.Paon;
import fr.afpa.entite.Poulet;
import fr.afpa.entite.Volaille;
import fr.afpa.entite.VolailleAbattable;
import fr.afpa.service.Service;
import fr.afpa.service.ServiceFerme;
import junit.framework.TestCase;

public class TestEleveur extends TestCase{
	
	Map<String, Volaille> liste;
	FermeElevage fermeElevage;
	
	@BeforeEach
	public void setUp() throws Exception {
		Service.initialisation();
	}

	@AfterEach
	public void tearDown() throws Exception {
		liste = null;
		fermeElevage = null;
	}

	@Test
	public void testLivraisonDeVolailleOK() {
		fermeElevage = new FermeElevage();
		liste = new HashMap<String, Volaille>();
		Volaille poulet1 = new Poulet(5.0f);
		Volaille poulet2 = new Poulet(5.0f);
		Volaille poulet3 = new Poulet(5.0f);
		Volaille canard1 = new Canard(5.0f);
		Volaille canard2 = new Canard(5.0f);
		Volaille paon1 = new Paon();
		Volaille paon2 = new Paon();
		liste.put(poulet1.getNumeroIdentifiant(), poulet1);
		liste.put(poulet2.getNumeroIdentifiant(), poulet2);
		liste.put(poulet3.getNumeroIdentifiant(), poulet3);
		liste.put(canard1.getNumeroIdentifiant(), canard1);
		liste.put(canard2.getNumeroIdentifiant(), canard2);
		liste.put(paon1.getNumeroIdentifiant(), paon1);
		liste.put(paon2.getNumeroIdentifiant(), paon2);
		new ServiceFerme().livraisonDeVolaille(fermeElevage, liste);
		Service.initialisation();
		assertTrue(new ServiceFerme().livraisonDeVolaille(fermeElevage, liste));
	}

	@Test
	public void testLivraisonDeVolailleKO() {
		fermeElevage = new FermeElevage();
		liste = new HashMap<String, Volaille>();
		Volaille poulet1 = new Poulet(5.0f);
		Volaille poulet2 = new Poulet(5.0f);
		Volaille poulet3 = new Poulet(5.0f);
		Volaille canard1 = new Canard(5.0f);
		Volaille canard2 = new Canard(5.0f);
		Volaille paon1 = new Paon();
		Volaille paon2 = new Paon();
		liste.put(poulet1.getNumeroIdentifiant(), poulet1);
		liste.put(poulet2.getNumeroIdentifiant(), poulet2);
		liste.put(poulet3.getNumeroIdentifiant(), poulet3);
		liste.put(canard1.getNumeroIdentifiant(), canard1);
		liste.put(canard2.getNumeroIdentifiant(), canard2);
		liste.put(paon1.getNumeroIdentifiant(), paon1);
		liste.put(paon2.getNumeroIdentifiant(), paon2);
		new ServiceFerme().livraisonDeVolaille(fermeElevage, liste);
		Volaille paon3 = new Paon();
		liste.put(paon3.getNumeroIdentifiant(), paon3);
		assertFalse(new ServiceFerme().livraisonDeVolaille(fermeElevage, liste));
	}
	
	@Test
	public void testNombreVolailleParType() {
		fermeElevage = new FermeElevage();
		liste = new HashMap<String, Volaille>();
		Volaille poulet1 = new Poulet(5.0f);
		Volaille poulet2 = new Poulet(5.0f);
		Volaille poulet3 = new Poulet(5.0f);
		Volaille canard1 = new Canard(5.0f);
		Volaille canard2 = new Canard(5.0f);
		Volaille paon1 = new Paon();
		Volaille paon2 = new Paon();
		liste.put(poulet1.getNumeroIdentifiant(), poulet1);
		liste.put(poulet2.getNumeroIdentifiant(), poulet2);
		liste.put(poulet3.getNumeroIdentifiant(), poulet3);
		liste.put(canard1.getNumeroIdentifiant(), canard1);
		liste.put(canard2.getNumeroIdentifiant(), canard2);
		liste.put(paon1.getNumeroIdentifiant(), paon1);
		liste.put(paon2.getNumeroIdentifiant(), paon2);
		new ServiceFerme().livraisonDeVolaille(fermeElevage, liste);
		assertEquals("3-2-2",new ServiceFerme().nombreVolailleParType(fermeElevage));
	}
	
	@Test
	public void testCalculerPrixVolailleAbattable() {
		fermeElevage = new FermeElevage();
		liste = new HashMap<String, Volaille>();
		Volaille poulet1 = new Poulet(5.0f);
		Volaille poulet2 = new Poulet(5.0f);
		Volaille poulet3 = new Poulet(5.0f);
		Volaille canard1 = new Canard(5.0f);
		Volaille canard2 = new Canard(5.0f);
		Volaille paon1 = new Paon();
		Volaille paon2 = new Paon();
		liste.put(poulet1.getNumeroIdentifiant(), poulet1);
		liste.put(poulet2.getNumeroIdentifiant(), poulet2);
		liste.put(poulet3.getNumeroIdentifiant(), poulet3);
		liste.put(canard1.getNumeroIdentifiant(), canard1);
		liste.put(canard2.getNumeroIdentifiant(), canard2);
		liste.put(paon1.getNumeroIdentifiant(), paon1);
		liste.put(paon2.getNumeroIdentifiant(), paon2);
		new ServiceFerme().livraisonDeVolaille(fermeElevage, liste);
		Poulet.setPrixPoulet(2);
		Canard.setPrixCanard(1);
		assertEquals(40,new ServiceFerme().calculerPrixVolailleAbattable(fermeElevage));
	}
	
	@Test
	public void testVendreUneVolaille() {
		fermeElevage = new FermeElevage();
		liste = new HashMap<String, Volaille>();
		Volaille poulet = new Poulet(25.0f);
		liste.put(poulet.getNumeroIdentifiant(), poulet);
		new ServiceFerme().livraisonDeVolaille(fermeElevage, liste);
		assertEquals(1, fermeElevage.getVolailles().size());
		System.out.println(fermeElevage);
		assertTrue(new ServiceFerme().vendreUneVolaille(fermeElevage, poulet.getNumeroIdentifiant()));
		assertEquals(0, fermeElevage.getVolailles().size());
		assertFalse(new ServiceFerme().vendreUneVolaille(fermeElevage, poulet.getNumeroIdentifiant()));
		assertEquals(0, fermeElevage.getVolailles().size());
	}
	
	@Test
	public void testRendreUnPaon() {
		fermeElevage = new FermeElevage();
		liste = new HashMap<String, Volaille>();
		Volaille poulet1 = new Poulet(5.0f);
		Volaille poulet2 = new Poulet(5.0f);
		Volaille poulet3 = new Poulet(5.0f);
		Volaille canard1 = new Canard(5.0f);
		Volaille canard2 = new Canard(5.0f);
		Volaille paon1 = new Paon();
		Volaille paon2 = new Paon();
		liste.put(poulet1.getNumeroIdentifiant(), poulet1);
		liste.put(poulet2.getNumeroIdentifiant(), poulet2);
		liste.put(poulet3.getNumeroIdentifiant(), poulet3);
		liste.put(canard1.getNumeroIdentifiant(), canard1);
		liste.put(canard2.getNumeroIdentifiant(), canard2);
		liste.put(paon1.getNumeroIdentifiant(), paon1);
		liste.put(paon2.getNumeroIdentifiant(), paon2);
		new ServiceFerme().livraisonDeVolaille(fermeElevage, liste);
		assertEquals(7, fermeElevage.getVolailles().size());
		assertTrue(new ServiceFerme().rendreUnPaon(fermeElevage, paon1.getNumeroIdentifiant()));
		assertEquals(6, fermeElevage.getVolailles().size());
		assertFalse(new ServiceFerme().rendreUnPaon(fermeElevage, paon1.getNumeroIdentifiant()));
		assertEquals(6, fermeElevage.getVolailles().size());
	}
}
