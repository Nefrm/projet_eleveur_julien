package fr.afpa.test;

import java.util.Scanner;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import fr.afpa.entite.FermeElevage;
import fr.afpa.entite.Paon;
import fr.afpa.entite.Poulet;
import fr.afpa.entite.Volaille;
import fr.afpa.menu.Menu;
import junit.framework.TestCase;

public class TestMenu extends TestCase{

	FermeElevage fermeElevage;

	@BeforeEach
	public void setUp() throws Exception {
		fermeElevage = new FermeElevage();
	}

	@AfterEach
	public void tearDown() throws Exception {
	}

	@Test
	public void testChoix1() {
		assertTrue(Menu.choix1(fermeElevage, new Scanner("1\n1\n0\n10\n10\n")));
		assertFalse(Menu.choix1(fermeElevage, new Scanner("8\n0\n0\n10\n10\n10\n10\n10\n10\n10\n10\n")));

	}

	@Test
	public void testChoix2() {
		assertTrue(Menu.choix2('p', 10));
		assertFalse(Menu.choix2('p', -10));
		assertTrue(Menu.choix2('c', 10));
		assertFalse(Menu.choix2('c', -10));
		assertFalse(Menu.choix2('a', 10));
	}

	@Test
	public void testChoix3() {
		assertTrue(Menu.choix3('p', 10));
		assertFalse(Menu.choix3('p', -10));
		assertTrue(Menu.choix3('c', 10));
		assertFalse(Menu.choix3('c', -10));
		assertFalse(Menu.choix3('a', 10));
	}

	@Test
	public void testChoix4() {
		Volaille poulet = new Poulet(10);
		fermeElevage.getVolailles().put(poulet.getNumeroIdentifiant(), poulet);
		assertTrue(Menu.choix4(poulet, 5));
	}
	
	@Test
	public void testChoix7() {
		Volaille poulet = new Poulet(10);
		fermeElevage.getVolailles().put(poulet.getNumeroIdentifiant(), poulet);
		assertTrue(Menu.choix7(fermeElevage, poulet.getNumeroIdentifiant()));
		((Poulet) poulet).modifierPoidsAbattage(20);
		Volaille poulet1 = new Poulet(10);
		fermeElevage.getVolailles().put(poulet1.getNumeroIdentifiant(), poulet1);
		assertFalse(Menu.choix7(fermeElevage, poulet1.getNumeroIdentifiant()));
	}

	@Test
	public void testChoix8() {
		Volaille paon = new Paon();
		fermeElevage.getVolailles().put(paon.getNumeroIdentifiant(), paon);
		assertTrue(Menu.choix8(fermeElevage, paon.getNumeroIdentifiant()));
	}

}
