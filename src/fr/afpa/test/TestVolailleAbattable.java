package fr.afpa.test;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import fr.afpa.entite.Canard;
import fr.afpa.entite.Poulet;
import fr.afpa.entite.VolailleAbattable;
import fr.afpa.service.Service;
import junit.framework.TestCase;

public class TestVolailleAbattable extends TestCase{

	VolailleAbattable poulet;
	VolailleAbattable canard;
	
	@BeforeEach
	public void setUp() throws Exception {
		poulet = new Poulet();
		canard = new Canard();
		Service.initialisation();
	}

	@AfterEach
	public void tearDown() throws Exception {
	}

	@Test
	public void testModifierPoidsOKPoulet() {
		assertTrue(poulet.modifierPoids(10.0f));
	}

	@Test
	public void testModifierPoidsKOPoulet() {
		assertFalse(poulet.modifierPoids(-10.0f));
	}
	
	@Test
	public void testModifierPoidsOKCanard() {
		assertTrue(canard.modifierPoids(10.0f));
	}

	@Test
	public void testModifierPoidsKOCanard() {
		assertFalse(canard.modifierPoids(-10.0f));
	}
}
