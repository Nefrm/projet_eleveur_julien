package fr.afpa.test;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import fr.afpa.entite.Canard;
import fr.afpa.service.Service;
import junit.framework.TestCase;

public class TestCanard extends TestCase{
	Canard canard;
	
	@BeforeEach
	public void setUp() throws Exception {
		canard = new Canard();
		Service.initialisation();
	}

	@AfterEach
	public void tearDown() throws Exception {
	}

	@Test
	public void testChangerPrixOK() {
		assertTrue(canard.changerPrix(10.0));
	}
	
	@Test
	public void testChangerPrixKO() {
		assertFalse(canard.changerPrix(-10.0));
	}

	@Test
	public void testModifierPoidsAbattableOK() {
		assertTrue(canard.modifierPoidsAbattage(10.0f));
	}
	
	@Test
	public void testModifierPoidsAbattableKO() {
		assertFalse(canard.modifierPoidsAbattage(-10.0f));
	}
}
