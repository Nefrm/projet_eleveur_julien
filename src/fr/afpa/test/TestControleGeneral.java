package fr.afpa.test;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import fr.afpa.controle.ControleGeneral;
import fr.afpa.entite.Canard;
import fr.afpa.entite.Paon;
import fr.afpa.entite.Poulet;
import fr.afpa.entite.Volaille;
import fr.afpa.service.Service;
import junit.framework.TestCase;

public class TestControleGeneral extends TestCase{

	Map<String, Volaille> liste;
	
	@BeforeEach
	public void setUp() throws Exception {
		liste = new HashMap<String, Volaille>();
		Volaille poulet1 = new Poulet(5.0f);
		Volaille poulet2 = new Poulet(5.0f);
		Volaille poulet3 = new Poulet(5.0f);
		Volaille canard1 = new Canard(5.0f);
		Volaille canard2 = new Canard(5.0f);
		Volaille paon1 = new Paon();
		Volaille paon2 = new Paon();
		liste.put(poulet1.getNumeroIdentifiant(), poulet1);
		liste.put(poulet2.getNumeroIdentifiant(), poulet2);
		liste.put(poulet3.getNumeroIdentifiant(), poulet3);
		liste.put(canard1.getNumeroIdentifiant(), canard1);
		liste.put(canard2.getNumeroIdentifiant(), canard2);
		liste.put(paon1.getNumeroIdentifiant(), paon1);
		liste.put(paon2.getNumeroIdentifiant(), paon2);
		Service.initialisation();
	}

	@AfterEach
	public void tearDown() throws Exception {
		liste = null;
	}

	@Test
	public void testLivraisonValideOK() {
		assertTrue(ControleGeneral.livraisonValide(liste));
	}
	
	@Test
	public void testLivraisonValideKO7Plus() {
		Volaille poulet4 = new Poulet(5.0f);
		Volaille poulet5 = new Poulet(5.0f);
		Volaille poulet6 = new Poulet(5.0f);
		liste.put(poulet4.getNumeroIdentifiant(), poulet4);
		liste.put(poulet5.getNumeroIdentifiant(), poulet5);
		liste.put(poulet6.getNumeroIdentifiant(), poulet6);
		assertFalse(ControleGeneral.livraisonValide(liste));
	}
	
	@Test
	public void testLivraisonValideKOVolaillePlus() {
		liste.remove("3");
		liste.remove("4");
		liste.remove("5");
		Volaille poulet4 = new Poulet(5.0f);
		Volaille poulet5 = new Poulet(5.0f);
		Volaille poulet6 = new Poulet(5.0f);
		liste.put(poulet4.getNumeroIdentifiant(), poulet4);
		liste.put(poulet5.getNumeroIdentifiant(), poulet5);
		liste.put(poulet6.getNumeroIdentifiant(), poulet6);
		assertFalse(ControleGeneral.livraisonValide(liste));
	}

	@Test
	public void testControlePoidsOK() {
		assertTrue(ControleGeneral.controlePoids(10.0f));
	}
	
	@Test
	public void testControlePoidsKO() {
		assertFalse(ControleGeneral.controlePoids(-10.0f));
	}
}
