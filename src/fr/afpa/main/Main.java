package fr.afpa.main;

import java.util.Scanner;

import fr.afpa.entite.FermeElevage;
import fr.afpa.menu.Menu;
import fr.afpa.service.Service;

public class Main {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		FermeElevage fermeElevage = new FermeElevage();
		Service.initialisation();
		int choix = -1;
		while(choix != 0) {
			Menu.afficherMenu();
			System.out.println("Votre choix :");
			choix = in.nextInt();in.nextLine();
			Menu.menu(fermeElevage, choix, in);
		}
		in.close();
	}

}
