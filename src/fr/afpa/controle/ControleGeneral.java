package fr.afpa.controle;

import java.util.Map;

import fr.afpa.entite.Canard;
import fr.afpa.entite.Paon;
import fr.afpa.entite.Poulet;
import fr.afpa.entite.Volaille;

public class ControleGeneral {
	private final static int MAX = 7;
	private final static int CANARD = 4;
	private final static int POULET = 5;
	private final static int PAON = 3;
	
	
	/**
	 * Verifie si la livraison est valide ou non
	 * 
	 * @param liste	la liste a verifier
	 * @return	true si la livraison est valide, false si non
	 */
	public static boolean livraisonValide(Map<String, Volaille> liste) {
		if (liste.size() > MAX){
			System.out.println("le nombre de volaille doit etre inferieur �" + MAX);
			return false;
		}
		int canard = 0;
		int poulet = 0;
		int paon = 0;
		for (Map.Entry<String, Volaille> volaille : liste.entrySet()) {
			if(volaille.getValue() instanceof Canard)
				canard++;
			else if(volaille.getValue() instanceof Poulet)
				poulet++;
			else if(volaille.getValue() instanceof Paon)
				paon++;
			if(canard > CANARD) {
				System.out.println("le nombre de canard doit etre inferieur �" + CANARD);
				return false;
			}
			if(poulet > POULET){
				System.out.println("le nombre de poulet doit etre inferieur �" + POULET);
				return false;
			}
			if(paon > PAON){
				System.out.println("le nombre de paon doit etre inferieur �" + PAON);
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Controle le poids en parametre
	 * 
	 * @param poids	le poids a verifier
	 * @return true si le poids est correcte, false si non
	 */
	public static boolean controlePoids(float poids) {
		if (poids >= 0)
			return true;
		return false;
	}
}
