package fr.afpa.menu;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import fr.afpa.entite.Canard;
import fr.afpa.entite.FermeElevage;
import fr.afpa.entite.Paon;
import fr.afpa.entite.Poulet;
import fr.afpa.entite.Volaille;
import fr.afpa.entite.VolailleAbattable;
import fr.afpa.service.Service;
import fr.afpa.service.ServiceFerme;

public class Menu {

	/**
	 * Affiche le menu disponible pour l'eleveur
	 */
	public static void afficherMenu() {
		System.out.println("------------------------------Menu------------------------------");
		System.out.println(" 1- Ajouter des volailles");
		System.out.println(" 2- Modifier le poids pour l'abattage");
		System.out.println(" 3- Modifier le prix du jour");
		System.out.println(" 4- Modifier le poids d'une volaille");
		System.out.println(" 5- Voir le nombre de volaille par type");
		System.out.println(" 6- Voir le total de prix des volailles abattables");
		System.out.println(" 7- Vendre une volaille");
		System.out.println(" 8- Rendre un paon");
		System.out.println(" 9- Visualiser l'etat de la ferme");
		System.out.println(" 0- Quitter");
		System.out.println("----------------------------------------------------------------");
	}

	/**
	 * demande un choix du menu
	 * 
	 * @param choix        le choix de l'utilisateur
	 * @param fermeElevage l'eleveur
	 * @param in           le scanner
	 */
	public static void menu(FermeElevage fermeElevage, int choix, Scanner in) {
		switch (choix) {
		case 1: // Ajout de volaille
			choix1(fermeElevage, in);
			break;
		case 2: // Modification de poids d'abattage
			System.out.println("(P)oulet ou (C)anard ?");
			char c2 = in.nextLine().toLowerCase().charAt(0);
			System.out.println("Nouveau poids :");
			String poids2 = in.next();
			Float poidsV = Float.valueOf(poids2);
			in.nextLine();
			choix2(c2, poidsV);
			break;
		case 3: // Modification de prix
			System.out.println("(P)oulet ou (C)anard ?");
			char c3 = in.nextLine().toLowerCase().charAt(0);
			System.out.println("Nouveau prix :");
			double prix3 = in.nextDouble();
			in.nextLine();
			choix3(c3, prix3);
			break;
		case 4: // Modification de poids
			System.out.println("saisir le numero d'identifiant de la volaille :");
			Volaille volaille = fermeElevage.getOneVolaille(in.nextLine());
			System.out.println("Nouveau poids :");
			float poids4 = in.nextFloat();
			in.nextLine();
			choix4(volaille, poids4);
			break;
		case 5: // Voir le nombre de volaille par type
			choix5(fermeElevage);
			break;
		case 6: // Voir le total de prix des volailles abattables
			choix6(fermeElevage);
			break;
		case 7: // Vendre une volaille
			System.out.println("saisir le numero d'identifiant de la volaille :");
			String numero7 = in.nextLine();
			choix7(fermeElevage, numero7);
			break;
		case 8: // Rendre un paon

			System.out.println("saisir le numero d'identifiant du paon :");
			String numero8 = in.nextLine();
			choix8(fermeElevage, numero8);
			break;
		case 9: // afficher l'etat de la ferme
			choix9(fermeElevage);
			break;
		default:
			break;
		}
	}

	/**
	 * choix 1 du menu : ajout de volaille
	 * 
	 * @param fermeElevage
	 * @param in           le scanner
	 * @return true si le choix a ete effectuer
	 */
	public static boolean choix1(FermeElevage fermeElevage, Scanner in) {
		return new ServiceFerme().livraisonDeVolaille(fermeElevage, Service.creationListeLivraison(in));
	}

	/**
	 * choix 2 du menu : modification de poids de l'abattage
	 * 
	 * @param c le scanner
	 */
	public static boolean choix2(char c, float poids) {
		if (c == 'p') {
			if (new Poulet().modifierPoidsAbattage(poids)) {
				System.out.println("changement effectuer");
				return true;
			}
			System.out.println("changement echouer");
			return false;
		} else if (c == 'c') {
			if (new Canard().modifierPoidsAbattage(poids)) {
				System.out.println("changement effectuer");
				return true;
			}
			System.out.println("changement echouer");
			return false;
		}
		System.out.println("mauvais choix");
		return false;
	}

	/**
	 * choix 3 du menu : modification de prix
	 * 
	 * @param in le scanner
	 */
	public static boolean choix3(char c, double prix) {
		if (c == 'p') {
			if (new Poulet().changerPrix(prix)) {
				System.out.println("changement effectuer");
				return true;
			}
			System.out.println("changement echouer");
			return false;
		} else if (c == 'c') {
			if (new Canard().changerPrix(prix)) {
				System.out.println("changement effectuer");
				return true;
			}
			System.out.println("changement echouer");
			return false;
		}
		System.out.println("mauvais choix");
		return false;
	}

	/**
	 * choix 4 du menu : modification de poids
	 * 
	 * @param fermeElevage l'eleveur
	 * @param in           le scanner
	 */
	public static boolean choix4(Volaille volaille, float poids) {
		if (volaille != null && !(volaille instanceof Paon)) {
			return ((VolailleAbattable) volaille).modifierPoids(poids);
		}
		return false;
	}

	/**
	 * choix 5 du menu : nombre de volaille par type
	 * 
	 * @param fermeElevage l'eleveur
	 */
	public static void choix5(FermeElevage fermeElevage) {
		String[] prix = new ServiceFerme().nombreVolailleParType(fermeElevage).split("-");
		System.out.println("Poulet : " + prix[0]);
		System.out.println("Canard : " + prix[1]);
		System.out.println("Paon : " + prix[2]);
	}

	/**
	 * choix 6 du menu : voir le total de prix
	 * 
	 * @param fermeElevage l'eleveur
	 */
	public static void choix6(FermeElevage fermeElevage) {
		System.out.println("total du prix des volailles abattables :" + new ServiceFerme().calculerPrixVolailleAbattable(fermeElevage));
	}

	/**
	 * choix 7 du menu : vendre une volaille
	 * 
	 * @param fermeElevage l'eleveur
	 * @param in           le scanner
	 */
	public static boolean choix7(FermeElevage fermeElevage, String numeroIdentifiant) {
		Volaille volaille = fermeElevage.getOneVolaille(numeroIdentifiant);
		if (volaille != null && !(volaille instanceof Paon)) {
			if (new ServiceFerme().vendreUneVolaille(fermeElevage, volaille.getNumeroIdentifiant())) {
				System.out.println("volaille vendu");
				return true;
			}
			System.out.println("volaille invendable");
			return false;
		}
		System.out.println("volaille introuvable");
		return false;
	}

	/**
	 * choix 8 du menu : rendre un paon
	 * 
	 * @param fermeElevage l'eleveur
	 * @param in           le scanner
	 */
	public static boolean choix8(FermeElevage fermeElevage, String numeroIdentifiant) {
		Volaille volaille = fermeElevage.getOneVolaille(numeroIdentifiant);
		if (volaille != null && volaille instanceof Paon) {
			new ServiceFerme().rendreUnPaon(fermeElevage, volaille.getNumeroIdentifiant());
			System.out.println("paon rendu");
			return true;
		}
		System.out.println("paon non trouver");
		return false;
	}

	/**
	 * Affiche l'etat de la ferme
	 * 
	 * @param fermeElevage la ferme en question
	 */
	public static void choix9(FermeElevage fermeElevage) {
		int canard = 0;
		int poulet = 0;
		int paon = 0;
		List<String> numCanard = new ArrayList<String>();
		List<String> numPoulet = new ArrayList<String>();
		List<String> numPaon = new ArrayList<String>();
		// Calcul du nombre de caractere
		for (Map.Entry<String, Volaille> volaille : fermeElevage.getVolailles().entrySet()) {
			if (volaille.getValue() instanceof Canard) {
				if (((Canard) volaille.getValue()).getPoids() > Canard.getPoidsAbattage()) {
					canard += 3; // CC_
				}
				else {
					canard += 2; // C_
				}
				numCanard.add(volaille.getKey());
			} else if (volaille.getValue() instanceof Poulet) {
				if (((Poulet) volaille.getValue()).getPoids() > Poulet.getPoidsAbattage()) {
					poulet += 3; // PP_
				}
				else {
					poulet += 2; // P_
				}
				numPoulet.add(volaille.getKey());
			} else if (volaille.getValue() instanceof Paon) {
				paon += 3; // Pa_
				numPaon.add(volaille.getKey());
			}
		}
		// Calcul des longueurs
		int longueurCanard = (int) Math.ceil(Math.sqrt(canard));
		int longueurPoulet = (int) Math.ceil(Math.sqrt(poulet));
		//int longueurPaon = canard + 1 + poulet;
		int longueurPaon = (int) Math.ceil(Math.sqrt(poulet)+1+Math.sqrt(canard));
		// garde de la hauteur
		int hauteur = (longueurCanard < longueurPoulet) ? longueurPoulet : longueurCanard;

		// Calcul de la hauteur des paons
		int hauteurPaon = (int) Math.ceil((double) paon / (double) longueurPaon);

		// Affichage
		// 1e ligne
		System.out.print("|");
		for (int i = 0; i < longueurPaon; i++) {
			System.out.print("-");
		}
		System.out.println("|");
		// n lignes
		int indiceCanard = 0;
		int indicePoulet = 0;
		int indicePaon = 0;
		for (int i = 0; i < hauteur; i+=2) {
			System.out.print("|");
			for (int j = 0; j < longueurCanard; j++) {
				if(numCanard.size() > indiceCanard && ((VolailleAbattable) fermeElevage.getOneVolaille(numPoulet.get(indiceCanard))) != null && ((VolailleAbattable) fermeElevage.getOneVolaille(numCanard.get(indiceCanard))).getPoids() < Canard.getPoidsAbattage())	//abattable
					System.out.print("C ");
				else if(numCanard.size() > indiceCanard && ((VolailleAbattable) fermeElevage.getOneVolaille(numPoulet.get(indiceCanard))) != null && ((VolailleAbattable) fermeElevage.getOneVolaille(numCanard.get(indiceCanard))).getPoids() >= Canard.getPoidsAbattage()) {
					System.out.print("CC ");
					j++;
				}
				else if(numCanard.size() <= indiceCanard)
					System.out.print(" ");
				indiceCanard++;
			}
			System.out.print("|");
			for (int j = 0; j < longueurPoulet; j++) {
				if(numPoulet.size() > indicePoulet && ((VolailleAbattable) fermeElevage.getOneVolaille(numPoulet.get(indicePoulet))) != null && ((VolailleAbattable) fermeElevage.getOneVolaille(numPoulet.get(indicePoulet))).getPoids() < Poulet.getPoidsAbattage())	//abattable
					System.out.print("P ");
				else if(numPoulet.size() > indicePoulet && ((VolailleAbattable) fermeElevage.getOneVolaille(numPoulet.get(indicePoulet))) != null && ((VolailleAbattable) fermeElevage.getOneVolaille(numPoulet.get(indicePoulet))).getPoids() >= Poulet.getPoidsAbattage()) {
					System.out.print("PP ");
					j++;
				}
				else if(numPoulet.size() <= indicePoulet)
					System.out.print(" ");
				indicePoulet++;
			}
			System.out.println("|");
		}
		// ligne milieu
		System.out.print("|");
		for (int i = 0; i < longueurPaon; i++) {
			System.out.print("-");
		}
		System.out.println("|");
		// ligne paon
		for (int i = 0; i < hauteurPaon; i++) {
			System.out.print("|");
			for (int j = 0; j < longueurPaon; j++) {
				if(numPaon.size() > indicePaon && fermeElevage.getOneVolaille(numPaon.get(indicePaon++)) != null) {	//abattable
					System.out.print("Pa ");
					j+=2;
				}
				else
					System.out.print(" ");
			}
			System.out.println("|");
		}
		// derniere ligne
		System.out.print("|");
		for (int i = 0; i < longueurPaon; i++) {
			System.out.print("-");
		}
		System.out.println("|");

	}
}
