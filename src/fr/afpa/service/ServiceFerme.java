package fr.afpa.service;

import java.io.File;
import java.util.Map;

import fr.afpa.controle.ControleGeneral;
import fr.afpa.entite.Canard;
import fr.afpa.entite.FermeElevage;
import fr.afpa.entite.Paon;
import fr.afpa.entite.Poulet;
import fr.afpa.entite.Volaille;
import fr.afpa.entite.VolailleAbattable;

public class ServiceFerme {

	/**
	 * ajoute une nouvelle livraison de volaille
	 * 
	 * @param liste la liste des nouvelles volailles
	 * @return true si la livraison a ete effectuer, false si non
	 */
	public boolean livraisonDeVolaille(FermeElevage fermeElevage, Map<String, Volaille> liste) {
		if (!ControleGeneral.livraisonValide(liste))
			return false;
		fermeElevage.getVolailles().putAll(liste);
		Service.enregistrement(liste);
		return true;
	}

	/**
	 * recupere le nombre de volaille par type
	 * 
	 * @return une chaine de charactere sous la forme : nbrpoulet-nbrcanard-nbrpaon
	 */
	public String nombreVolailleParType(FermeElevage fermeElevage) {
		if (fermeElevage.getVolailles() == null)
			return null;
		int canard = 0;
		int poulet = 0;
		int paon = 0;
		for (Map.Entry<String, Volaille> entry : fermeElevage.getVolailles().entrySet()) {
			if (entry.getValue() instanceof Canard)
				canard++;
			else if (entry.getValue() instanceof Poulet)
				poulet++;
			else if (entry.getValue() instanceof Paon)
				paon++;
		}
		return "" + poulet + "-" + canard + "-" + paon;
	}

	/**
	 * calcul le prix des volailles abattables
	 * 
	 * @return le prix des volailles abattables
	 */
	public int calculerPrixVolailleAbattable(FermeElevage fermeElevage) {
		int total = 0;
		for (Map.Entry<String, Volaille> volaille : fermeElevage.getVolailles().entrySet()) {
			if (volaille.getValue() instanceof Canard)
				total += ((VolailleAbattable) volaille.getValue()).getPoids()*Canard.getPrixCanard();
			else if (volaille.getValue() instanceof Poulet)
				total += ((VolailleAbattable) volaille.getValue()).getPoids()*Poulet.getPrixPoulet();
		}
		return total;
	}

	/**
	 * vend une volaille
	 * 
	 * @param numeroIdentification le numero de la volaille
	 * @return true si la volaille a ete vendu, false si non
	 */
	public boolean vendreUneVolaille(FermeElevage fermeElevage, String numeroIdentification) {
		Volaille v = fermeElevage.getVolailles().get(numeroIdentification);
		if(fermeElevage.getVolailles().containsKey(numeroIdentification) && !(v instanceof Paon)) {
			if(v instanceof Poulet && ((Poulet) v).getPoids() >= Poulet.getPoidsAbattage()) {
				Service.ajoutOperation(v, 'v', Poulet.getPrixPoulet()*((Poulet)v).getPoids(), ((Poulet) v).getPoids());
			}
			else if(v instanceof Canard && ((Canard) v).getPoids() >= Canard.getPoidsAbattage()) {
				Service.ajoutOperation(v, 'v', Canard.getPrixCanard()*((Canard) v).getPoids(), ((Canard) v).getPoids());
			}
			else{
				return false;
			}
			fermeElevage.getVolailles().remove(numeroIdentification);
			new File("ressource\\"+numeroIdentification+".txt").delete();
			return true;
		}return false;

	}

	/**
	 * Rend un Paon au parc
	 * 
	 * @param numeroIdentification	le numero du paon
	 * @return true si le paon a ete rendu, false si non
	 */
	public boolean rendreUnPaon(FermeElevage fermeElevage, String numeroIdentification) {
		Volaille v = fermeElevage.getVolailles().get(numeroIdentification);
		if(fermeElevage.getVolailles().containsKey(numeroIdentification) && v instanceof Paon) {
			Service.ajoutOperation(v);
			fermeElevage.getVolailles().remove(numeroIdentification);
			new File("ressource\\"+numeroIdentification+".txt").delete();
			return true;
		}
		return false;
	}
}
