package fr.afpa.service;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import fr.afpa.controle.ControleGeneral;
import fr.afpa.entite.Canard;
import fr.afpa.entite.Paon;
import fr.afpa.entite.Poulet;
import fr.afpa.entite.Volaille;
import fr.afpa.entite.VolailleAbattable;

public class Service {
	/**
	 * creer un nouveau dossier
	 * 
	 * @param chemin	le chemin du dossier
	 * @return	true si le dossier est creer, false si non
	 */
	public static boolean creationDossier(String chemin) {
		if(!new File(chemin).exists()) {
			new File(chemin).mkdir();
			return true;
		}
		return false;
	}
	
	/**
	 * Ajoute une operation dans la liste
	 * 
	 * @param volaille la volaille a operer
	 * @param operation le type d'operation
	 * @param prix le prix de la transaction
	 * @param poids le poids de la volaille
	 * @return true si l'operation a ete effectuer, false si non
	 */
	public static boolean ajoutOperation(Volaille volaille, char operation, double prix, float poids) {
		initialisation();
		DataOutputStream dos = null;
		try {
			dos = new DataOutputStream(new FileOutputStream("ressource\\operations.txt",true));
			dos.writeUTF(LocalDate.now().toString());
			dos.writeChar(operation);
			dos.writeUTF(volaille.getNumeroIdentifiant());
			dos.writeInt(((VolailleAbattable) volaille).getAge());
			dos.writeDouble(prix);
			dos.writeFloat(poids);
		} catch (FileNotFoundException e) {
			System.out.println("fichier non trouver");
			return false;
		} catch (IOException e) {
			System.out.println("probleme d'entree / sortie");
			return false;
		}finally {
			try {
				dos.close();
			} catch (IOException e) {
				System.out.println("probleme d'entree / sortie");
				return false;
			}
		}
		return true;
	}
	
	/**
	 * ajoute l'operation dans le fichier operations.txt
	 * 
	 * @param volaille	la volaille concern�
	 * @return	true si l'operation a ete effectuer false si non
	 */
	public static boolean ajoutOperation(Volaille volaille) {
		initialisation();
		DataOutputStream dos = null;
		try {
			dos = new DataOutputStream(new FileOutputStream("ressource\\operations.txt",true));
			dos.writeUTF(LocalDate.now().toString());
			dos.writeChar('r');
			dos.writeUTF(volaille.getNumeroIdentifiant());
		} catch (FileNotFoundException e) {
			System.out.println("fichier non trouver");
			return false;
		} catch (IOException e) {
			System.out.println("probleme d'entree / sortie");
			return false;
		}finally {
			try {
				dos.close();
			} catch (IOException e) {
				System.out.println("probleme d'entree / sortie");
				return false;
			}
		}
		return true;
	}
	
	/**
	 * initialise le dossier ressource
	 */
	public static void initialisation() {
		creationDossier("ressource");
	}
	
	/**
	 * Creer une nouvelle liste de livraison
	 * 
	 * @param in	le scanner
	 * @return une nouvelle liste de livraison
	 */
	public static Map<String, Volaille> creationListeLivraison(Scanner in){
		int poulet = 0;
		int canard = 0;
		int paon = 0;
		System.out.println("nombre de poulet :");
		poulet = in.nextInt();
		in.nextLine();
		System.out.println("nombre de canard :");
		canard = in.nextInt();
		in.nextLine();
		System.out.println("nombre de paon :");
		paon = in.nextInt();
		in.nextLine();
		Map<String, Volaille> nouvelleListe = new HashMap<String, Volaille>();
		for (int i = 1; i <= poulet; i++) {
			float poids = -1;
			while(!ControleGeneral.controlePoids(poids)) {
				System.out.println("entrer le poids du poulet n°"+i);
				poids = in.nextFloat();in.nextLine();
			}
			Volaille poulet1 = new Poulet(poids);
			nouvelleListe.put(poulet1.getNumeroIdentifiant(), poulet1);
		}
		for (int i = 1; i <= canard; i++) {
			float poids = -1;
			while(!ControleGeneral.controlePoids(poids)) {
				System.out.println("entrer le poids du canard n°"+i);
				poids = in.nextFloat();in.nextLine();
			}
			Volaille canard1 = new Canard(poids);
			nouvelleListe.put(canard1.getNumeroIdentifiant(), canard1);
		}
		for (int i = 1; i <= paon; i++) {
			Volaille paon1 = new Paon();
			nouvelleListe.put(paon1.getNumeroIdentifiant(), paon1);
		}
		return nouvelleListe;
	}

	/**
	 * enregistre les volaille dans le systeme
	 * 
	 * @param liste	la liste de volaille
	 */
	public static void enregistrement(Map<String, Volaille> liste) {
		for (Map.Entry<String, Volaille> volaille : liste.entrySet()) {
			ObjectOutputStream oos = null;
			try {
				oos = new ObjectOutputStream(new FileOutputStream("ressource\\"+volaille.getKey()+".txt"));
				oos.writeObject(volaille.getValue());
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally {
				try {
					oos.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * met a jour une volaille dans le systeme
	 * 
	 * @param volaille la volaille
	 */
	public static void enregistrement(Volaille volaille) {
		ObjectOutputStream oos = null;
		try {
			oos = new ObjectOutputStream(new FileOutputStream("ressource\\"+volaille.getNumeroIdentifiant()+".txt"));
			oos.writeObject(volaille);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			try {
				oos.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
