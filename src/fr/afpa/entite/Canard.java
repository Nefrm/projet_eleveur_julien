package fr.afpa.entite;

import fr.afpa.interf.Abattable;

public final class Canard extends VolailleAbattable implements Abattable {
	//Attribut(s)
	protected static double prixCanard;
	protected static float poidsAbattageCanard;

	//Constructeur(s)
	/**
	 * Constructeur par default
	 */
	public Canard() {
	}
	
	/**
	 * Constructeur normal
	 * 
	 * @param poids	le poids du nouveau canard
	 * @param numeroIdentifiant	le numero d'identifiant du nouveau canard
	 */
	public Canard(float poids) {
		this.age = 3;
		this.numeroIdentifiant = "CA"+idIncrement++;
		this.poids = poids;
	}

	//Getter(s)
	/**
	 * renvoi le prix des canard
	 * 
	 */
	public static double getPrixCanard() {
		return prixCanard;
		
	}
	
	/**
	 * renvoi le poids d'abattage
	 * 
	 * @return le poids d'abattage du canard
	 */
	public static float getPoidsAbattage() {
		return poidsAbattageCanard;
	}
	
	//Setter(s)
	/**
	 * modifie le prix des canard
	 * 
	 * @param i	le nouveau prix
	 */
	public static void setPrixCanard(int i) {
		prixCanard = i;
		
	}
	
	
	//Methode(s) herite
	@Override
	public boolean changerPrix(double prix) {
		if(prix < 0)
			return false;
		prixCanard = prix;
		return true;
	}
	
	@Override
	public boolean modifierPoidsAbattage(float poids) {
		if(poids < 0)
			return false;
		poidsAbattageCanard = poids;
		return true;
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		return true;
	}

}
