package fr.afpa.entite;

public final class Paon extends Volaille {
	
	public Paon() {
		this.numeroIdentifiant = "PA"+idIncrement++;
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		return true;
	}

}
