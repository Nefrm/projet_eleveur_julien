package fr.afpa.entite;

import java.io.Serializable;

public abstract class Volaille implements Serializable {
	// Attribut(s)
	protected static int idIncrement;
	protected String numeroIdentifiant;

	// Getter(s)
	/**
	 * Getter du numero identifiant
	 * 
	 * @return le numero d'identifiant
	 */
	public String getNumeroIdentifiant() {
		return this.numeroIdentifiant;
	}

	public int getIdIncrement() {
		return idIncrement;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((numeroIdentifiant == null) ? 0 : numeroIdentifiant.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Volaille other = (Volaille) obj;
		if (numeroIdentifiant == null) {
			if (other.numeroIdentifiant != null)
				return false;
		} else if (!numeroIdentifiant.equals(other.numeroIdentifiant))
			return false;
		return true;
	}
	
	
}
