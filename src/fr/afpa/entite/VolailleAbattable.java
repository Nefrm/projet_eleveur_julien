package fr.afpa.entite;

import fr.afpa.service.Service;

public abstract class VolailleAbattable extends Volaille {
	// Attribut(s)
	protected final int AGEMIN = 3;
	protected float poids;
	protected int age;

	// Getter(s)
	/**
	 * renvoi l'age
	 * 
	 * @return l'age de la volaille
	 */
	public int getAge() {
		return this.age;
	}

	/**
	 * renvoi le poids de la volaille
	 * 
	 * @return le poids en float
	 */
	public float getPoids() {
		return this.poids;
	}

	// Setter(s)
	/**
	 * modifie l'age
	 * 
	 * @param age le nouvel age
	 */
	public void setAge(int age) {
		this.age = age;
	}

	/**
	 * modifie le poids
	 * 
	 * @param poids le nouveau poids
	 */
	public void setPoids(float poids) {
		this.poids = poids;
	}

	// Methode(s)
	/**
	 * modifie le poids d'une volaille a abattre
	 * 
	 * @param volaille la volaille
	 * @return true si le poids a ete modifier, false si non
	 */
	public final boolean modifierPoids(float poids) {
		if (poids < 0) {
			System.out.println("poids invalide");
			return false;
		}
		this.setPoids(poids);
		System.out.println("modification effectuer");
		Service.enregistrement(this);
		return true;
	}

	// Methode(s) abstraite
	/**
	 * modifie le poids d'une volaille a abattre
	 * 
	 * @param volaille la volaille
	 * @return true si le poids a ete modifier, false si non
	 */
	public abstract boolean modifierPoidsAbattage(float poids);

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + AGEMIN;
		result = prime * result + age;
		result = prime * result + Float.floatToIntBits(poids);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		VolailleAbattable other = (VolailleAbattable) obj;
		if (AGEMIN != other.AGEMIN)
			return false;
		if (age != other.age)
			return false;
		if (Float.floatToIntBits(poids) != Float.floatToIntBits(other.poids))
			return false;
		return true;
	}
}
