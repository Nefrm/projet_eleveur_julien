package fr.afpa.entite;

import fr.afpa.interf.Abattable;

public final class Poulet extends VolailleAbattable implements Abattable {
	//Attribut(s)
	protected static double prixPoulet;
	protected static float poidsAbattagePoulet;
	
	//Constructeur(s)
	/**
	 * Constructeur par default
	 */
	public Poulet() {
	}
	
	/**
	 * Constructeur normal
	 * 
	 * @param poids le poids du nouveau poulet
	 * @param numeroIdentifiant le numero d'identifiant du nouveau poulet
	 */
	public Poulet(float poids) {
		this.age = 3;
		this.poids = poids;
		this.numeroIdentifiant = "PO"+idIncrement++;
	}

	@Override
	public boolean changerPrix(double prix) {
		if(prix < 0)
			return false;
		prixPoulet = prix;
		return true;
	}
	
	@Override
	public boolean modifierPoidsAbattage(float poids) {
		if(poids < 0)
			return false;
		poidsAbattagePoulet = poids;
		return true;
	}

	/**
	 * modifier le prix du poulet
	 * 
	 * @param i	le nouveau prix
	 */
	public static void setPrixPoulet(int i) {
		prixPoulet = i;
		
	}
	
	/**
	 * renvoi le poids d'abattage
	 * 
	 * @return le poids d'abattage du poulet
	 */
	public static float getPoidsAbattage() {
		return poidsAbattagePoulet;
	}
	
	public static double getPrixPoulet() {
		return prixPoulet;
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		return true;
	}
}
