package fr.afpa.entite;

import java.util.HashMap;
import java.util.Map;

import fr.afpa.controle.ControleGeneral;
import fr.afpa.service.Service;

final public class FermeElevage {
	// Attribut(s)
	private Map<String, Volaille> volailles;

	/**
	 * cr�er une nouvelle ferme
	 */
	public FermeElevage() {
		this.volailles = new HashMap<String, Volaille>();
	}

	// Getter(s)
	/**
	 * @return the volailles
	 */
	public Map<String, Volaille> getVolailles() {
		return volailles;
	}

	/**
	 * retourne une volaille
	 * 
	 * @param index l'index de la volaille
	 * @return une volaille
	 */
	public Volaille getOneVolaille(String key) {
		return this.volailles.get(key);
	}

	// Setter(s)
	/**
	 * @param volailles the volailles to set
	 */
	public void setVolailles(Map<String, Volaille> volailles) {
		this.volailles = volailles;
	}

	/**
	 * modifie une volaille
	 * 
	 * @param volaille la nouvelle volaille
	 * @param index    l'index de la nouvelle volaille
	 */
	public void setOneVolaille(Volaille volaille, String key) {
		this.volailles.replace(key, volaille);
	}

	@Override
	public String toString() {
		return "Eleveur [volailles=" + volailles + "]";
	}

	// M�thode(s)
	
}
